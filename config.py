DEBUG = False # False - пишет логи в файл, True - отображает конф.диалог (аналог --m при запуске)
AUTOEDIT = True # Следить за нарушителями (аналог -AE при запуске)
interface = 'enp0s8' # Интерфейс на котором висит DHCP-сервер
dhcpConfFile = '/etc/dhcp/dhcpd.conf'

fourthActet = '40-90'
netmask = '255.255.255.0'
defaultLeaseTime = 10
maxLeaseTime = 10
domainNameServers = '8.8.8.8'
domainName = 'workgroup'
