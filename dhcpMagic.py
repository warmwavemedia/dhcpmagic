# coding: UTF-8
# Python-скрипт для "предупреждения вторжения" в локальную сеть по средствам DHCP-сервера.

import subprocess
import logging
import random
import sys

import leasesList
from config import DEBUG, AUTOEDIT, dhcpConfFile, interface, netmask, defaultLeaseTime, maxLeaseTime, \
    domainNameServers, domainName, fourthActet
import config

if '--m' in sys.argv:
    DEBUG = True

if '-AE' in sys.argv:
    AUTOEDIT = True

if not DEBUG:
    logging.basicConfig(
        format=u'%(filename)-18s [LINE:%(lineno)-4s] %(levelname)-8s [%(asctime)s] %(message)s',
        level=logging.INFO,
        filename='debug.log'
    )


def macsStorage(action, leasers, filename):
    if action == 'r':
        with open(filename, 'r') as f:
            leasers = f.read().split('\n')

            if not DEBUG:
                logging.info(f'Сохранённых mac-адресов: {len(leasers) - 1}шт')
            else:
                print(f'--- Сохранённых mac-адресов: {len(leasers) - 1}шт')

        return leasers

    elif action == 'w':
        with open(filename, 'w') as f:
            for l in leasers:
                f.write(f"{l['hardware']},{l['ip_address']},{l['starts']},{l['client-hostname']}\n")
        if not DEBUG:
            logging.info(f'Сохранили mac-адреса({len(leasers)}шт)')
        else:
            print(f'--- Сохранили mac-адреса({len(leasers)}шт)')

    elif action == 'a':
        with open(filename, 'a') as f:
            for l in leasers:
                f.write(f"{l['hardware']},{l['ip_address']},{l['starts']},{l['client-hostname']}\n")
        if not DEBUG:
            logging.info(f'Добавили mac-адреса({len(leasers)}шт)')
        else:
            print(f'--- Добавили mac-адреса({len(leasers)}шт)')


def updateDhcp(updateDhcpData):
    idSubnet = updateDhcpData['idSubnet']
    defaultLeaseTime = updateDhcpData['defaultLeaseTime']
    intruders = updateDhcpData['intruders']
    fourthActet = updateDhcpData['fourthActet']

    ipRange = f'192.168.{idSubnet}.{fourthActet[0]} 192.168.{idSubnet}.{fourthActet[1]}'
    routersIP = f'192.168.{idSubnet}.9'

    fh = None
    try:
        fh = open(dhcpConfFile, 'w', encoding='utf-8')

        print('authoritative;', file=fh)
        print(f'default-lease-time {defaultLeaseTime};', file=fh)
        # print(f'max-lease-time {defaultLeaseTime};', file=fh)
        print(f'max-lease-time {defaultLeaseTime + 1};', file=fh)
        print(f'min-lease-time {defaultLeaseTime - 1};', file=fh)
        print('log-facility local7;', file=fh)
        # print('one-lease-per-client true;', file=fh)
        # print('allow bootp;', file=fh)
        print('', file=fh)

        intrudersList = []
        if len(intruders) >= 1:
            for position, m in enumerate(intruders):
                if m['hardware'] not in intrudersList:
                    intrudersList.append(m['hardware'])
                    print(f"host intruder_{m['client-hostname']} {{", file=fh)
                    print(f"    hardware ethernet {m['hardware']};", file=fh)
                    print(f"    ignore booting;", file=fh)
                    print('}', file=fh)
                    print('', file=fh)

        print(f'subnet 192.168.{idSubnet}.0 netmask {netmask} {{', file=fh)
        print(f'    range {ipRange};', file=fh)
        print(f'    option domain-name-servers {domainNameServers};', file=fh)
        print(f'    option domain-name "{domainName}";', file=fh)
        print(f'    option routers {routersIP};', file=fh)
        print(f'    option time-servers 192.168.{idSubnet}.9;', file=fh)
        print(f'    option broadcast-address 192.168.{idSubnet}.255;', file=fh)
        print('}', file=fh)

    finally:
        if fh:
            fh.close()

    if not DEBUG:
        logging.info(f'Конфигурация DHCP обновлена ({dhcpConfFile}).')
    else:
        print(f'--- Конфигурация DHCP обновлена ({dhcpConfFile}).')

    subprocess.call(['ifconfig', f'{interface}', f'192.168.{idSubnet}.9'])
    if not DEBUG:
        logging.info(f'Обновили настройки интерфейса ({interface})')
    else:
        print(f'--- Обновили настройки интерфейса ({interface})')

    subprocess.call(['service', 'isc-dhcp-server', 'stop'])
    subprocess.call(['rm', '-rf', '/var/lib/dhcp/dhcpd.leases'])
    subprocess.call(['touch', '/var/lib/dhcp/dhcpd.leases'])
    subprocess.call(['service', 'isc-dhcp-server', 'restart'])

    if not DEBUG:
        logging.info('Сервис DHCP перезапущен.')
    else:
        print('--- Сервис DHCP перезапущен.')


def main():
    subprocess.run('clear', shell=True)
    savedMacs = macsStorage('r', None, 'leases.csv')

    try:
        leasers = leasesList.main()
    except:
        leasers = []

    intruders = []

    for l in leasers:
        matching = 0
        for m in savedMacs:
            if str(l['hardware']) == str(m.split(',')[0]):
                matching += 1

        if matching == 0:
            intruders.append(l)

    idSubnetRandom = int(random.randint(11, 254))
    idSubnet = idSubnetRandom
    fourthActet = list([random.randint(11, 127), random.randint(127, 254)])
    defaultLeaseTime = config.defaultLeaseTime

    # MENU
    if '--m' in sys.argv:
        leasesList.printLeases(leasers, leasesList.timestamp_now())

        if not DEBUG:
            logging.info(f'Выданных mac-адресов: {len(leasers)}шт')
        else:
            print(f'--- Выданных mac-адреса: {len(leasers)}шт')

        if len(intruders) >= 1:
            if not DEBUG:
                logging.info(f'ОБНАРУЖЕНЫ НАРУШИТЕЛИ: {len(intruders)}')
            else:
                print(f'--- ОБНАРУЖЕНЫ НАРУШИТЕЛИ: {len(intruders)}')
            leasesList.printLeases(intruders, leasesList.timestamp_now())

        saveMacs = str(input('Сохранить mac-адреса клиентов, все свои(y/N)?: ')).lower()
        if saveMacs == 'y':
            macsStorage('w', leasers, 'leases.csv')
            savedMacs = leasers

        # TODO Тут меню выбора intruder
        else:
            pass

        try:
            idSubnet = int(input(f'Введите подсеть и/или Enter (по умолчанию - {idSubnetRandom}): '))
        except:
            idSubnet = idSubnetRandom

        fourthActetOLD = config.fourthActet.split('-')
        fourthActet = input(
            f'Введите диапазон IP и/или Enter (по умолчанию - {fourthActetOLD[0]}-{fourthActetOLD[1]}): ').split('-')
        if len(fourthActet) == 1:
            fourthActet = fourthActetOLD

        defaultLeaseTimeOLD = defaultLeaseTime
        try:
            defaultLeaseTime = int(
                input(f'Введите время обновления IP и/или Enter (в конфиге - {defaultLeaseTime}sec): '))
            logging.info(f'Время обновления IP (в конфиге - {defaultLeaseTimeOLD}: {defaultLeaseTime}sec): ')
        except:
            defaultLeaseTime = defaultLeaseTimeOLD
            logging.info(f'Время обновления (из конфига): {defaultLeaseTime}sec')

    updateDhcpData = {}
    updateDhcpData['idSubnet'] = idSubnet
    updateDhcpData['defaultLeaseTime'] = defaultLeaseTime
    updateDhcpData['intruders'] = intruders
    updateDhcpData['savedMacs'] = savedMacs
    updateDhcpData['fourthActet'] = fourthActet

    if AUTOEDIT or len(intruders) >= 1 or '--m' in sys.argv:
        updateDhcp(updateDhcpData)

        if not DEBUG:
            logging.info(f'IP адреса клиентов обновлены.')
        print(f'--- IP адреса клиентов обновлены.')


if __name__ == '__main__':
    main()
